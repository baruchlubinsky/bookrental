from django.test import TestCase, Client
from django.urls import reverse

class TestRental(TestCase): 

  def test_route(self):
    c = Client()
    response = c.get('/rentals/')
    self.assertEqual(response.status_code, 200)
  
  def test_valid_post(self):
    c = Client()
    response = c.post(reverse('receipt'), {'title_0': 'a book', 'duration_0': 25, 'category_0': 'Novels', 'total': 1})
    self.assertEqual(response.status_code, 200)
  
  def test_incorrect_number_of_titles(self):
    c = Client()
    response = c.post(reverse('receipt'), {'duration_0': 25, 'category_0': 'Novels', 'total': 1})
    self.assertEqual(response.status_code, 400)

  def test_incorrect_number_of_durations(self):
    c = Client()
    response = c.post(reverse('receipt'), {'title_0': 'a book', 'category_0': 'Novels', 'total': 1})
    self.assertEqual(response.status_code, 400)

  def test_incorrect_number_of_categories(self):
    c = Client()
    response = c.post(reverse('receipt'), {'title_0': 'a book', 'duration_0': 3, 'total': 1})
    self.assertEqual(response.status_code, 400)

  def test_valid_post(self):
    c = Client()
    response = c.post(reverse('receipt'), {'title_0': 'a book', 'duration_0': 25, 'category_0': 'Novels', 'title_1': 'another book', 'duration_1': 5, 'category_1': 'Fiction', 'total': 2})
    self.assertEqual(response.status_code, 200)

  def test_incorrect_number_of_durations_2(self):
    c = Client()
    response = c.post(reverse('receipt'), {'title_0': 'a book', 'duration_0': 25, 'category_0': 'Novels', 'title_1': 'another book', 'category_1': 'Fiction', 'total': 2})
    self.assertEqual(response.status_code, 400)
  
  def test_incorrect_number_of_titles_2(self):
    c = Client()
    response = c.post(reverse('receipt'), {'title_0': 'a book', 'duration_0': 25, 'category_0': 'Novels', 'duration_1': 1, 'category_1': 'Fiction', 'total': 2})
    self.assertEqual(response.status_code, 400)

  def test_response_total_1(self):
    c = Client()
    response = c.post(reverse('receipt'), {'title_0': 'a book', 'duration_0': 25, 'category_0': 'Fiction', 'total': 1})
    self.assertEqual(response.status_code, 200)

    rows = len(response.context['rows'])
    total = response.context['total']
    
    self.assertEqual(rows, 1)
    self.assertEqual(total, 75.0)

  def test_response_total_2(self):
    c = Client()
    response = c.post(reverse('receipt'), {'title_0': 'a book', 'duration_0': 25, 'category_0': 'Fiction', 'title_1': 'another book', 'duration_1': 5, 'category_1': 'Novels', 'total': 2})
    self.assertEqual(response.status_code, 200)

    rows = len(response.context['rows'])
    total = response.context['total']

    self.assertEqual(rows, 2)
    self.assertEqual(total, 82.5)

  def test_response_total_3(self):
    c = Client()
    response = c.post(reverse('receipt'), {'title_0': 'a book', 'duration_0': 1, 'category_0': 'Regular', 'title_1': 'another book', 'duration_1': 1, 'category_1': 'Novels', 'total': 2})
    self.assertEqual(response.status_code, 200)

    rows = len(response.context['rows'])
    total = response.context['total']

    self.assertEqual(rows, 2)
    self.assertEqual(total, 6.5)

