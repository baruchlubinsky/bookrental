from django.test import TestCase
from rentals.views import calculateCharge

class TestRates(TestCase):

  def test_regular_min(self):
    self.assertEqual(calculateCharge("Regular", 1), 2.0)

  def test_regular_general(self):
    self.assertEqual(calculateCharge("Regular", 4), 5.0)

  def test_fiction(self):
    self.assertEqual(calculateCharge("Fiction", 1), 3.0)

  def test_novels_min(self):
    self.assertEqual(calculateCharge("Novels", 1), 4.5)

  def test_novels_general(self):
    self.assertEqual(calculateCharge("Novels", 4), 6.0)
  