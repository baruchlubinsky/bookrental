from django.http import HttpResponseBadRequest
from django.shortcuts import render, HttpResponse

# Create your views here.
def index(request):
  return render(request, 'rentals/index.html')

def create(request):
  try:
    count = int(request.POST['total'])
    data = []
    total = 0

    for i in range(0, count):
      days = int(request.POST[f'duration_{i}'])
      category = request.POST[f'category_{i}']
      charge =  calculateCharge(category, days)
      data.append({'title': request.POST[f'title_{i}'], 'charge': charge})
      total = total + charge
  
  except KeyError:
      return HttpResponseBadRequest("POST requires `title`, `duration` and `category` fields for each rental.")
  else:
      return render(request, 'rentals/receipt.html', {'rows': data, 'total': total})
    
def calculateCharge(category, duration):
  if category == "Novels":
    if duration <= 3:
      return 4.5
    else:
      return 1.5 * duration
  if category == "Fiction":
    return 3.0 * duration
  if duration <= 2:
    return 2.0
  else:
    return 2.0 + (1.5 * (duration - 2))