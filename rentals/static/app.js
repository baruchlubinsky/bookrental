function add_book() {
  var div = document.getElementById("books");
  var counter = document.getElementById("counter");
  var x = parseInt(counter.getAttribute("value"));
  
  var form = document.createElement("p");
  var category = document.createElement("select");
  category.name = 'category_' + x;
  var regular = document.createElement("option");
  regular.value = "Regular";
  regular.text = "Regular";
  var fiction = document.createElement("option");
  fiction.value = "Fiction";
  fiction.text = "Fiction";
  var novels = document.createElement("option");
  novels.value = "Novels";
  novels.text = "Novels";
  category.appendChild(regular);
  category.appendChild(fiction);
  category.appendChild(novels);

  var rentLabel = document.createElement("label");
  rentLabel.innerHTML = "Rent"; 
  var title = document.createElement("input");
  title.type = "text";
  title.name = "title_" + x;
  var forLabel = document.createElement("label");
  forLabel.innerHTML = "for"; 
  var duration = document.createElement("input");
  duration.type = "number";
  duration.name = "duration_" + x;
  var daysLabel = document.createElement("label");
  daysLabel.innerHTML = "days."; 

  form.appendChild(category);
  form.appendChild(rentLabel);
  form.appendChild(title);
  form.appendChild(forLabel);
  form.appendChild(duration);
  form.appendChild(daysLabel);

  div.appendChild(form);

  counter.setAttribute("value", x+1);

  // <p>
  // <label>Rent</label>
  // <input type="text" name="title">
  // <label>for</label>
  // <input type="number" name="duration"> 
  // <label>days.</label>
  // </p>`
}